package li.ues.topfriends

import android.content.Context
import android.graphics.Bitmap
import li.ues.topfriends.utils.UserData
import li.ues.topfriends.utils.WebkitCookieManagerProxy

import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.net.URLEncoder
import java.util.HashMap
import java.util.regex.Pattern
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.toBitmap


object Utils {
    var TYPE_JSON = "application/json"
    var TYPE_FORM = "application/x-www-form-urlencoded"
    var UserAgent =
        "Mozilla/5.0 (Linux; Android 8.1.0; Topfriends; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/75.0.3770.101 Mobile Safari/537.36"

    fun bootHttpClient() {
        val coreCookieManager = WebkitCookieManagerProxy(null, java.net.CookiePolicy.ACCEPT_ALL)
        java.net.CookieHandler.setDefault(coreCookieManager)
    }


    /**
     * THIS IS SO WRONG
     * @param params
     * @return
     */
    private fun getQuery(params: HashMap<String, String>): String {
        try {
            val result = StringBuilder()
            var first = true

            for ((key, value) in params) {
                if (first)
                    first = false
                else
                    result.append("&")

                result.append(URLEncoder.encode(key, "UTF-8"))
                result.append("=")
                result.append(URLEncoder.encode(value, "UTF-8"))
            }

            return result.toString()
        } catch (ex: Exception) {
            return ""
        }

    }

    fun rawPostUrl(uri: String, type: String, data: String): String? {
        bootHttpClient()
        var url: URL? = null

        try {
            url = URL(uri)
            val connection = url.openConnection() as HttpURLConnection
            connection.setRequestProperty("User-Agent", UserAgent)
            connection.requestMethod = "POST"
            connection.setRequestProperty("Content-Type", type)
            connection.doInput = true
            connection.connectTimeout = 10000
            connection.doOutput = true

            val os = connection.outputStream
            val pw = PrintWriter(OutputStreamWriter(os))
            pw.write(data)
            pw.close()

            connection.connect()

            val `in` = InputStreamReader(connection.content as InputStream)
            val buff = BufferedReader(`in`)
            val text = StringBuilder()

            for (l in buff.readLines()) {
                text.append(l)
            }

            return text.toString()
        } catch (e: MalformedURLException) {
            return null
        } catch (e: IOException) {
            return null
        }

    }

    fun newRawPostUrl(uri: String, type: String, data: String): String? {
        bootHttpClient()
        var url: URL? = null

        try {
            url = URL(uri)
            val connection = url.openConnection() as HttpURLConnection
            connection.setRequestProperty("User-Agent", UserAgent)
            connection.requestMethod = "POST"
            connection.setRequestProperty("Content-Type", type)
            connection.doInput = true
            connection.connectTimeout = 10000
            connection.doOutput = true

            val os = connection.outputStream
            val pw = PrintWriter(OutputStreamWriter(os))
            pw.write(data)
            pw.close()

            connection.connect()

            val `in` = InputStreamReader(connection.content as InputStream)
            val buff = BufferedReader(`in`)
            val text = StringBuilder()

            for (l in buff.readLines()) {
                text.append(l)
            }

            return text.toString()
        } catch (e: MalformedURLException) {
            return null
        } catch (e: IOException) {
            return null
        }

    }

    fun getImage(url: String): Bitmap {
        val inputStream = URL(url).content as InputStream
        return Drawable.createFromStream(inputStream, "file").toBitmap()
    }

    @JvmOverloads
    fun getUrl(uri: String, redirections: Int? = 0): String? {
        bootHttpClient()
        var url: URL? = null

        try {
            url = URL(uri)
            val connection = url.openConnection() as HttpURLConnection
            connection.setRequestProperty("User-Agent", UserAgent)
            connection.connectTimeout = 5000
            connection.connect()
            val status = connection.responseCode
            var redirect: Boolean? = false
            if (status != HttpURLConnection.HTTP_OK) {
                if (status == HttpURLConnection.HTTP_MOVED_TEMP
                    || status == HttpURLConnection.HTTP_MOVED_PERM
                    || status == HttpURLConnection.HTTP_SEE_OTHER
                )
                    redirect = true
            }

            if (redirect!!) {
                val nextLocation = connection.getHeaderField("Location")
                return if (nextLocation == null) {
                    throw MalformedURLException()
                } else {
                    getUrl(nextLocation, redirections!! + 1)
                }
            }

            val `in` = InputStreamReader(connection.content as InputStream)
            val buff = BufferedReader(`in`)
            val text = StringBuilder()

            for (l in buff.readLines()) {
                text.append(l)
            }

            return text.toString()
        } catch (e: MalformedURLException) {
            return null
        } catch (e: IOException) {
            return null
        }

    }

    fun isUserOnHtml(html: String): UserData? {
        var userId: String? = ""
        var fbDtsg: String? = ""

        try {
            val p = Pattern.compile("\"USER_ID\":\"(.*?)\"")
            val m = p.matcher(html)
            if (m.find()) {
                userId = m.group(1)
            }

            val p2 = Pattern.compile("\"token\":\"(.*?)\"", Pattern.MULTILINE)
            val m2 = p2.matcher(html)
            while (m2.find()) {
                val v = m2.group(1)
                if (v.contains(":")) {
                    fbDtsg = v
                    break;
                }
            }

            return UserData(userId!!, fbDtsg!!)
        } catch (ex: Exception) {
            ex.printStackTrace()
            return UserData("", "")
        }


    }

    fun plural(ctx: Context, count: Long, singular: Int, plural: Int, none: Int): String {
        return if (count > 1L) {
            String.format(ctx.getString(plural), count)
        } else if (count == 1L) {
            String.format(ctx.getString(singular), count)
        } else {
            String.format(ctx.getString(none), count)
        }
    }
}
