package li.ues.topfriends

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.crashlytics.android.Crashlytics
import li.ues.topfriends.system.ImageMeta
import li.ues.topfriends.system.Thread

import java.io.Serializable
import com.google.android.gms.common.util.IOUtils.toByteArray
import li.ues.topfriends.system.ConfiguredImageMeta
import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream
import java.util.Base64.getEncoder



internal interface OnItemClickListener<T> {
    fun onClick(view: View, item: T)
}

internal class ArrayAdapterItem(var mContext: Context, var layoutResourceId: Int, data: List<Thread>) :
    ArrayAdapter<Thread>(mContext, layoutResourceId, data) {
    var items: List<Thread> = ArrayList()
    var listener: OnItemClickListener<Thread>? = null

    init {
        this.items = data
    }

    fun setOnItemClickListener(listener: OnItemClickListener<Thread>) {
        this.listener = listener
    }

    override fun getView(position: Int, originalView: View?, parent: ViewGroup): View {
        val v = when (originalView) {
            null -> {
                val inflater = (mContext as Activity).layoutInflater
                val inflated = inflater.inflate(layoutResourceId, parent, false)

                inflated.setTag(R.id.thread_picture, inflated.findViewById(R.id.thread_picture))
                inflated.setTag(R.id.thread_counter, inflated.findViewById(R.id.thread_counter))
                inflated.setTag(R.id.thread_name, inflated.findViewById(R.id.thread_name))

                inflated
            }
            else -> originalView
        }

        // object item based on the position
        val objectItem = items!![position]

        val profilePicture = v.getTag(R.id.thread_picture) as ImageView
        val counter = v.getTag(R.id.thread_counter) as TextView
        val name = v.getTag(R.id.thread_name) as TextView

        name.text = objectItem.OtherUser!!.ShortName

        var count = 0L
        if (objectItem.MessageCount != null) {
            count = objectItem.MessageCount!!
        }

        counter.text = Utils.plural(
            mContext,
            count,
            R.string.sing_messages,
            R.string.plur_messages,
            R.string.none_messages
        )

        Glide.with(mContext)
            .load(objectItem.OtherUser!!.ImageUrl)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .fitCenter()
            .apply(RequestOptions.circleCropTransform())
            .placeholder(R.drawable.logo)
            .into(profilePicture)

        //        // get the TextView and then set the text (item name) and tag (item ID) values
        //        TextView textViewItem = (TextView) convertView.findViewById(R.id.textViewItem);
        //        textViewItem.setText(objectItem.itemName);
        //        textViewItem.setTag(objectItem.itemId);

        v.setOnClickListener { view ->
            if (listener != null) {
                listener!!.onClick(view, objectItem)
            }
        }

        return v

    }

}

internal class WizardPagerAdapter(mContext: Context) : PagerAdapter() {

    private val mContext: Context? = mContext

    override fun destroyItem(container: View, position: Int, `object`: Any) {
        //((ViewPager) container).removeView((View) object);
    }

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        var resId = 0
        when (position) {
            0 -> resId = R.id.configurar_page_1
            1 -> resId = R.id.configurar_page_2
            2 -> resId = R.id.configurar_page_3
        }
        return collection.findViewById(resId)
    }

    override fun getCount(): Int {
        return 3
    }

    override fun isViewFromObject(arg0: View, arg1: Any): Boolean {
        return arg0 === arg1 as View
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return mContext!!.getString(R.string.configurar_tab_configurar)
            1 -> return mContext!!.getString(R.string.configurar_tab_selecionados)
            else -> return mContext!!.getString(R.string.configurar_tab_removidos)
        }
    }
}

class ConfigurarImagemActivity : GenericActivity() {

    private var meta: ImageMeta? = null

    private var adapter1: ArrayAdapterItem? = null
    private var adapter2: ArrayAdapterItem? = null

    // 3(4 colunas), 4(3 colunas) ou 6(2 colunas)
    private var columnSize: Long = 2L
    private var maxFriends: Long = 10L
    private var mostrarRanking: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configurar_imagem)

        logAction("Configurar imagem acessado")

        val intent = intent
        try {
            meta = intent.getSerializableExtra("data") as ImageMeta
        } catch (ex: Exception) {
            Crashlytics.logException(ex);
            Toast.makeText(
                applicationContext,
                getString(R.string.finalizar_generic_error),
                Toast.LENGTH_SHORT
            ).show()
            finish()
            return
        }

        val list = findViewById<View>(R.id.thread_list) as GridView
        val removedList = findViewById<View>(R.id.thread_list_removed) as GridView
        val finalizar = findViewById<View>(R.id.finalizar) as Button

        adapter1 = ArrayAdapterItem(this, R.layout.thread_view, meta!!.Threads)
        adapter2 = ArrayAdapterItem(this, R.layout.thread_view, meta!!.RemovedThreads)

        adapter2!!.setOnItemClickListener(object : OnItemClickListener<Thread> {
            override fun onClick(view: View, item: Thread) {
                adapter1!!.add(item)
                adapter1!!.sort(object : Comparator<Thread> {
                    override fun compare(a: Thread?, b: Thread?): Int {
                        if (a == null) {
                            return 0
                        }

                        if (b == null) {
                            return 0
                        }

                        return when {
                            a.MessageCount > b.MessageCount -> -1
                            a.MessageCount < b.MessageCount -> 1
                            else -> 0
                        }
                    }
                })
//                adapter1!!.sort(object : Ordering<Thread>() {
//                    override fun compare(left: Thread?, right: Thread?): Int {
//                        return Longs.compare(left!!.MessageCount!!, right!!.MessageCount!!)
//                    }
//                }.reverse())
                adapter2!!.remove(item)
            }
        })

        adapter1!!.setOnItemClickListener(object : OnItemClickListener<Thread> {
            override fun onClick(view: View, item: Thread) {
                adapter2!!.add(item)
                adapter2!!.sort(object : Comparator<Thread> {
                    override fun compare(a: Thread?, b: Thread?): Int {
                        if (a == null) {
                            return 0
                        }

                        if (b == null) {
                            return 0
                        }

                        return when {
                            a.MessageCount > b.MessageCount -> -1
                            a.MessageCount < b.MessageCount -> 1
                            else -> 0
                        }
                    }
                })
//                adapter2!!.sort(object : Ordering<Thread>() {
//                    override fun compare(left: Thread?, right: Thread?): Int {
//                        return Longs.compare(left!!.MessageCount!!, right!!.MessageCount!!)
//                    }
//                }.reverse())
                adapter1!!.remove(item)
            }
        })

        list.adapter = adapter1
        removedList.adapter = adapter2


        val adapter = WizardPagerAdapter(this)
        val pager = findViewById<View>(R.id.pager) as ViewPager
        pager.adapter = adapter
        pager.currentItem = 1

        finalizar.setOnClickListener {
            val configuredImageMeta = ConfiguredImageMeta(
                maxFriends,
                columnSize,
                mostrarRanking,
                ArrayList(adapter1!!.items)
            )
            logAction("Finalizar clicado")
            val newIntent = Intent(this@ConfigurarImagemActivity, FinalizarActivity::class.java)
            newIntent.putExtra("data", configuredImageMeta)
            startActivity(newIntent)
            finish()
        }

        val columnsRadios = arrayOf(R.id.duas_colunas, R.id.tres_colunas, R.id.quatro_colunas)
        val maxFriendsRadios = arrayOf(
            R.id.cinco_amigos,
            R.id.dez_amigos,
            R.id.quinze_amigos,
            R.id.vinte_amigos,
            R.id.vinte_e_cinco_amigos,
            R.id.trinta_amigos,
            R.id.quarenta_amigos
        )
        val showRankingRadios = arrayOf(R.id.mostrar_ranking_nao, R.id.mostrar_ranking_sim)

        for (radioId in columnsRadios) {
            val radio = findViewById<androidx.appcompat.widget.AppCompatRadioButton>(radioId)
            radio.setOnClickListener(this::onColumnRadioClicked)
        }

        for (radioId in maxFriendsRadios) {
            val radio = findViewById<androidx.appcompat.widget.AppCompatRadioButton>(radioId)
            radio.setOnClickListener(this::onMaxFriendsRadioClicked)
        }

        for (radioId in showRankingRadios) {
            val radio = findViewById<androidx.appcompat.widget.AppCompatRadioButton>(radioId)
            radio.setOnClickListener(this::onShowRankRadioClicked)
        }
    }

    private fun onColumnRadioClicked(view: View) {
        val checked = (view as RadioButton).isChecked

        when (view.getId()) {
            R.id.duas_colunas -> if (checked)
                this.columnSize = 2L
            R.id.tres_colunas -> if (checked)
                this.columnSize = 3L
            R.id.quatro_colunas -> if (checked)
                this.columnSize = 4L
        }
    }

    fun onMaxFriendsRadioClicked(view: View) {
        val checked = (view as RadioButton).isChecked

        when (view.getId()) {
            R.id.cinco_amigos -> if (checked)
                this.maxFriends = 5L
            R.id.dez_amigos -> if (checked)
                this.maxFriends = 10L
            R.id.quinze_amigos -> if (checked)
                this.maxFriends = 15L
            R.id.vinte_amigos -> if (checked)
                this.maxFriends = 20L
            R.id.vinte_e_cinco_amigos -> if (checked)
                this.maxFriends = 25L
            R.id.trinta_amigos -> if (checked)
                this.maxFriends = 30L
            R.id.quarenta_amigos -> if (checked)
                this.maxFriends = 40L
        }
    }

    fun onShowRankRadioClicked(view: View) {
        val checked = (view as RadioButton).isChecked

        when (view.getId()) {
            R.id.mostrar_ranking_sim -> if (checked)
                this.mostrarRanking = true
            R.id.mostrar_ranking_nao -> if (checked)
                this.mostrarRanking = false
        }
    }
}
