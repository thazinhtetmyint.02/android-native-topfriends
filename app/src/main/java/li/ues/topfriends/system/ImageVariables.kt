package li.ues.topfriends.system

internal data class ImageVariables(
    val lines: Long,
    val columns: Long,
    val limit: Long,
    val actualScale: Double = 1.0,
    val margin: Double = 6.0,
    val boxWidth: Double = 240 + (margin * 2),
    val pictureSize: Double = 60.0,
    val boxHeight: Double = pictureSize + (margin * 4),
    val headerHeight: Double = 40.0,
    val footerHeight: Double = 40.0,

    val targetHeight: Double = (lines * boxHeight) + (margin * 2) +
            headerHeight +
            footerHeight,

    val targetWidth: Double = (columns * boxWidth) + (margin * 2),

    val fontSmall: Double = 16.0,
    val fontMedium: Double = 17.0,
    val fontLarge: Double = 18.0,
    val positionSize: Double = boxHeight,
    val positionFontSize: Double = 90.0,
    val primaryColor: String = "#2a9fd6",
    val primaryBorderColor: String = "#2a9fd6",
    val primaryTextColor: String = "#ffffff",
    val backgroundColor: String = "#333",
    val headerTextColor: String = "#fff",
    val footerTextColor: String = "#fff"
) {
    companion object {
        fun fromMeta(meta: ConfiguredImageMeta): ImageVariables {
            val limit = meta.maxFriends
            val lines = Math.ceil(limit.toDouble() / meta.columns.toDouble())


            return ImageVariables(lines.toLong(), meta.columns, limit)
        }
    }

    fun scale(to: Double): ImageVariables =
        this.copy(
            actualScale = to,
            margin = margin * to,
            boxWidth = boxWidth * to,
            pictureSize = pictureSize * to,
            boxHeight = boxHeight * to,
            headerHeight = headerHeight * to,
            footerHeight = footerHeight * to,
            targetHeight = targetHeight * to,
            targetWidth = targetWidth * to,
            fontSmall = fontSmall * to,
            fontMedium = fontMedium * to,
            fontLarge = fontLarge * to,
            positionSize = positionSize * to,
            positionFontSize = positionFontSize * to
        )

    fun toMap(): Map<String, Any> =
        mapOf(
            "lines" to lines,
            "columns" to columns,
            "limit" to limit,
            "actualScale" to actualScale,
            "margin" to margin,
            "boxWidth" to boxWidth,
            "pictureSize" to pictureSize,
            "boxHeight" to boxHeight,
            "headerHeight" to headerHeight,
            "footerHeight" to footerHeight,
            "targetHeight" to targetHeight,
            "targetWidth" to targetWidth,
            "fontSmall" to fontSmall,
            "fontMedium" to fontMedium,
            "fontLarge" to fontLarge,
            "positionSize" to positionSize,
            "positionFontSize" to positionFontSize,
            "primaryColor" to primaryColor,
            "primaryBorderColor" to primaryBorderColor,
            "primaryTextColor" to primaryTextColor,
            "backgroundColor" to backgroundColor,
            "footerTextColor" to footerTextColor,
            "headerTextColor" to headerTextColor
        )

}