package li.ues.topfriends.system


import java.io.Serializable

data class Thread(
    var OtherUserId: String,
    var Participants: List<User>,
    var MessageCount: Long,
    var OtherUser: User
): Serializable {
    override fun toString(): String {
        return super.toString() + " " + String.format(
            "%s - %s - %d",
            OtherUser.Name,
            OtherUserId,
            MessageCount
        )
    }
}
