package li.ues.topfriends

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker

/**
 * Created by Wesley on 14/01/2016.
 */
@SuppressLint("Registered")
open class GenericFragmentActivity : FragmentActivity() {
    protected var mTracker: Tracker? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val application = application as TheApplication
        mTracker = application.defaultTracker

        mTracker!!.setScreenName("Loaded~" + this.javaClass.name)
        mTracker!!.send(HitBuilders.ScreenViewBuilder().build())
    }

    fun logAction(action: String) {
        mTracker!!.send(
            HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction(action)
                .build()
        )
    }

    fun logError(type: String) {
        mTracker!!.send(
            HitBuilders.EventBuilder()
                .setCategory("Error")
                .setAction(type)
                .build()
        )
    }
}
