package li.ues.topfriends

import android.content.Intent
import android.graphics.Bitmap
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.webkit.*
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import li.ues.topfriends.utils.UserData

/* An instance of this class will be registered as a JavaScript interface */
internal interface MyJavaScriptInterface {
    @JavascriptInterface
    fun processHTML(html: String)
}

internal open class AsyncCheckHtml : AsyncTask<String, Int, UserData?>() {
    override fun doInBackground(vararg params: String): UserData? {
        return Utils.isUserOnHtml(params[0])
    }

}

class EntrarNoFacebookActivity : GenericActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entrar_no_facebook)

        val logo = findViewById<View>(R.id.logo) as ImageView

        val animation = AnimationUtils.loadAnimation(applicationContext, R.anim.newanim)

        logo.animation = animation

        //CookieSyncManager.createInstance(this);
        //CookieManager.getInstance().removeAllCookie();

        val webview = findViewById<View>(R.id.entrarNoFacebookWebView) as WebView
        val status = findViewById<View>(R.id.entrarNoFacebookTextViewBottom) as TextView
        val loading = findViewById<View>(R.id.entrar_loading) as RelativeLayout

        val self = this

        webview.addJavascriptInterface(object : MyJavaScriptInterface {
            @JavascriptInterface
            override fun processHTML(html: String) {
                val task = object : AsyncCheckHtml() {
                    override fun onPreExecute() {
                        self.runOnUiThread {
                            try {
                                status.text = getString(R.string.entrar_pagina_carregada_procurando)
                            } catch (ex: Exception) {
                                println(ex.message)
                            }
                        }
                        super.onPreExecute()
                    }

                    override fun onPostExecute(data: UserData?) {
                        if (data!!.isOk) {
                            logAction("Usuario logado")

                            val userData = Intent()
                            userData.putExtra("id", data.id)
                            userData.putExtra("fbDtsg", data.fbDtsg)
                            setResult(-0x4e94ff4b, userData)

                            finish()
                        } else {
                            self.runOnUiThread {
                                loading.visibility = View.GONE
                                try {
                                    status.text = getString(R.string.entrar_pagina_carregada_aguardando)
                                } catch (ex: Exception) {
                                    println(ex.message)
                                }
                            }
                        }
                    }
                }

                task.execute(html)
            }
        }, "HTMLOUT")

        val ws = webview.settings
        ws.userAgentString = Utils.UserAgent
        ws.javaScriptEnabled = true

        webview.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                loading.visibility = View.VISIBLE

                super.onPageStarted(view, url, favicon)
                status.text = getString(R.string.entrar_pagina_carregando)
            }

            override fun onPageFinished(view: WebView, url: String) {
                /*Bitmap bitmap = Bitmap.createBitmap(webview.getWidth(), webview.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                webview.draw(canvas);*/

                super.onPageFinished(view, url)

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    CookieSyncManager.getInstance().sync()
                } else {
                    CookieManager.getInstance().flush();
                }

                status.text = getString(R.string.entrar_pagina_carregada_aguardando)
                webview.loadUrl("javascript:HTMLOUT.processHTML(document.documentElement.outerHTML);")
            }
        }
        webview.loadUrl("https://m.facebook.com/")

    }

}
